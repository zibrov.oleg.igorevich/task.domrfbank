<?php
declare(strict_types=1);

namespace App\File;

use RuntimeException;

class OfficesFile
{
    public const TYPE = 'office';
    public const PATH_FILE = '/upload/offices/';
    public const FILE_NAME = 'offices';
    public const FIELD_PHONE = 'phone';
    public const FIELD_STREET = 'street';
    private const ITEMS_ADDRESS = [
        'city', 'street', 'house', 'officeOrApartment'
    ];

    private array $arData;

    public function clearData(): void
    {
        $this->arData = [];
    }

    public function parseRow(string $row): void
    {
        if (strpos($row, ':') !== false) {
            $arStr = explode(':', $row, 2);
            $fieldName = trim($arStr[0]);
            $data = trim($arStr[1]);

            if (!empty($data) && self::FIELD_PHONE === $fieldName) {
                $data = $this->formatPhone($data);
            }

            $this->arData[$fieldName] = $data;
        }
    }

    private function formatPhone(string $phone): string
    {
        preg_match(
            '/[\+]?([7|8])[-|\s]?(\d{3})[-|\s]?(\d{3})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
            $phone,
            $matches
        );

        return !empty($matches) ?
            sprintf('%s(%s)%s-%s-%s', $matches[1], $matches[2], $matches[3], $matches[4], $matches[5]) : $phone;
    }

    public function getData(): array
    {
        return $this->arData;
    }

    public function formatDataInXml(array $arData): array
    {
        foreach ($arData as $key => $arItem) {
            if (!empty($arItem['name'])) {
                $arData[$key]['name'] = str_replace(['«', '»'], '', $arItem['name']);
            }

            if (!empty($arItem['address'])) {
                $address = explode(' офис ', $arItem['address']);
                if (count($address) === 2) {
                    $arData[$key]['address'] = $address[0];
                    $arData[$key]['office'] = 'офис ' . $address[1];
                }
            }
        }

        return $arData;
    }

    public function formatDataInJson(array $arData): array
    {
        $arDataJson = [
            'data' => []
        ];
        foreach ($arData as $key => $arItem) {
            $arDataJson['data'][$key] = [
                'type' => self::TYPE
            ];
            if (!empty($arItem['id'])) {
                $arDataJson['data'][$key]['id'] = (int)$arItem['id'];
            }
            if (!empty($arItem['name']) || !empty($arItem['address']) || !empty($arItem['phone'])) {
                $arDataJson['data'][$key]['attributes'] = [];
                if (!empty($arItem['name'])) {
                    $arDataJson['data'][$key]['attributes']['name'] = $arItem['name'];
                }
                if (!empty($arItem['address'])) {
                    $arDataJson['data'][$key]['attributes']['address'] = $this->formatAddress($arItem['address']);
                }
                if (!empty($arItem['phone'])) {
                    $arDataJson['data'][$key]['attributes']['phone'] = [
                        'countryNumber' => substr(
                            str_replace(['-', '+'], '', filter_var($arItem['phone'], FILTER_SANITIZE_NUMBER_INT)),
                            1
                        ),
                        'official' => $arItem['phone']
                    ];
                }
            }
        }

        return $arDataJson;
    }

    private function formatAddress(string $address): array
    {
        $arDataAddress = [];
        preg_match_all(
            '/(?<=г\.).*?(?=\s|$)|(?<=улица\s).*?(?=\sдом|$)|(?<=дом\s).*?(?=\sофис|$)|(?<=офис\s).*?(?=$)/',
            $address,
            $matches
        );
        if ($arMatches = current($matches)) {
            foreach (self::ITEMS_ADDRESS as $key => $item) {
                if (!empty($arMatches[$key]) && $item === self::FIELD_STREET) {
                    $arMatches[$key] = $this->formatStreet($arMatches[$key]);
                }

                $arDataAddress[$item] = $arMatches[$key] ?? null;
            }
        }

        return $arDataAddress;
    }

    private function formatStreet(string $street): string
    {
        if (strpos($street, 'проспект') !== false) {
            $newStreet = str_replace('проспект', 'пр-т', $street);
        } else {
            $newStreet = 'ул. ' . $street;
        }

        return $newStreet;
    }

    /**
     * @throws RuntimeException
     */
    public function createDirUpload(): void
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . self::PATH_FILE;
        if (!is_dir($dir) && !mkdir($dir, 0777, true) && !is_dir($dir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }
    }
}
