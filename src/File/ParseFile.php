<?php
declare(strict_types=1);

namespace App\File;

class ParseFile
{
    private array $arDataParsingFile = [];

    public function parsingFileOfficesTxt(string $pathFile, OfficesFile $obOfficesFile): void
    {
        $key = 0;
        $arRows = explode("\n", file_get_contents($pathFile));
        foreach ($arRows as $row) {
            if (!empty($row)) {
                $obOfficesFile->clearData();
                $obOfficesFile->parseRow($row);
                if (empty($this->arDataParsingFile[$key])) {
                    $this->arDataParsingFile[$key] = $obOfficesFile->getData();
                } else {
                    $this->arDataParsingFile[$key] = array_merge($this->arDataParsingFile[$key], $obOfficesFile->getData());
                }
            } else {
                $key++;
            }
        }
    }

    public function getDataParsingFile(): array
    {
        return $this->arDataParsingFile;
    }
}
