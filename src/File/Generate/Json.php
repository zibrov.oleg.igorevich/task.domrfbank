<?php
declare(strict_types=1);

namespace App\File\Generate;

use JsonException;

class Json extends FormatData
{
    public const TYPE_FILE = 'json';

    /**
     * @throws JsonException
     */
    public function generateFileOffices(array $arData, string $pathFile): void
    {
        $jsonData = json_encode($arData, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $jsonData = preg_replace('/^( +?)\\1(?=[^ ])/ m', '$1', $jsonData);

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $pathFile, $jsonData);
        chmod($_SERVER['DOCUMENT_ROOT'] . $pathFile, 0777);
        $this->pathFile = $pathFile;
    }
}