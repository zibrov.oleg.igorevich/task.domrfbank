<?php
declare(strict_types=1);

namespace App\File\Generate;

use XMLWriter;

class Xml extends FormatData
{
    public const TYPE_FILE = 'xml';

    /**
     * @throws XMLWriter
     */
    public function generateFileOffices(array $arData, string $pathFile): void
    {
        $fn = fopen($_SERVER['DOCUMENT_ROOT'] . $pathFile, 'wb');
        fclose($fn);

        $obXml = new XMLWriter();

        $obXml->openURI($_SERVER['DOCUMENT_ROOT'] . $pathFile);
        $obXml->startDocument('1.0', 'UTF-8');

        $obXml->startElement('companies');
        $obXml->setIndent(true);
        $obXml->setIndentString('    ');

        foreach ($arData as $asItem) {
            $obXml->startElement('company');

            if (!empty($asItem['id'])) {
                $obXml->writeElement('company-id', (string)$asItem['id']);
            }

            if (!empty($asItem['name'])) {
                $obXml->startElement('name');
                $obXml->writeAttribute('lang', 'ru');
                $obXml->text((string)$asItem['name']);
                $obXml->endElement();
            }

            if (!empty($asItem['phone'])) {
                $obXml->startElement('phone');
                $obXml->writeElement('type', 'phone');
                $obXml->writeElement('number', (string)$asItem['phone']);
                $obXml->endElement();
            }

            if (!empty($asItem['address'])) {
                $obXml->startElement('address');
                $obXml->writeAttribute('lang', 'ru');
                $obXml->text(PHP_EOL . '            ' . $asItem['address'] . PHP_EOL . '        ');
                $obXml->endElement();
            }

            if (!empty($asItem['office'])) {
                $obXml->writeElement('address-add', $asItem['office']);
            }

            $obXml->endElement();
        }

        $obXml->endElement();

        $obXml->endDocument();
        $obXml->flush();

        chmod($_SERVER['DOCUMENT_ROOT'] . $pathFile, 0777);
        $this->pathFile = $pathFile;
    }
}
