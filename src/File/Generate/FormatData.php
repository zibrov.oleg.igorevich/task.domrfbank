<?php
declare(strict_types=1);

namespace App\File\Generate;

abstract class FormatData
{
    protected string $pathFile = '';

    public function getPathFile(): string
    {
        return $this->pathFile;
    }

    public function getTypeFile(): string
    {
        return static::TYPE_FILE;
    }

    abstract public function generateFileOffices(array $arData, string $pathFile): void;
}
