<?php
declare(strict_types=1);

namespace App\File;

use App\File\Generate\FormatData;
use App\File\Generate\Json;
use App\File\Generate\Xml;

class TransformingFile
{
    private array $arDataParsingFile;
    private array $arPathConvertedFiles = [];

    public function convertFileOfficesTxt(string $pathFile, FormatData $obFormatData): void
    {
        $obOfficesFile = new OfficesFile();
        if (empty($this->arDataParsingFile)) {
            $obParseFile = new ParseFile();
            $obParseFile->parsingFileOfficesTxt($pathFile, $obOfficesFile);
            $this->arDataParsingFile = $obParseFile->getDataParsingFile();
        }

        if (!empty($this->arDataParsingFile)) {
            if ($obFormatData->getTypeFile() === Xml::TYPE_FILE) {
                $arData = $obOfficesFile->formatDataInXml($this->arDataParsingFile);
            } else if ($obFormatData->getTypeFile() === Json::TYPE_FILE) {
                $arData = $obOfficesFile->formatDataInJson($this->arDataParsingFile);
            }

            if (!empty($arData)) {
                $obOfficesFile->createDirUpload();
                $pathFile = OfficesFile::PATH_FILE . OfficesFile::FILE_NAME . '.' . $obFormatData->getTypeFile();
                $obFormatData->generateFileOffices($arData, $pathFile);
                $this->arPathConvertedFiles[$obFormatData->getTypeFile()] = $obFormatData->getPathFile();
            }
        }
    }

    public function getPathConvertedFiles(): array
    {
        return $this->arPathConvertedFiles;
    }
}
