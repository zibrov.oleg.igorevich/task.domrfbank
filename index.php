<?php
declare(strict_types=1);

require_once __DIR__ . '/composer/vendor/autoload.php';

use App\File\Generate\Json;
use App\File\Generate\Xml;
use App\File\TransformingFile;

?>

    <h1>Задание для кандидата на должность middle php разработчик.</h1>

    <p><a href="https://github.com/domrfbank/backend_task_middle">Ссылка на задание</a></p>

    <h3>Задача:</h3>
    <p>Написать код преобразующий офисы из текстового файла в json и xml.</p>

    <h3>Требования:</h3>
    <ul>
        <li>Объектно-ориентированный подход к решению задачи.</li>
        <li>Помни о SOLID.</li>
        <li>На вход программа получает путь к текстовому файлу.</li>
        <li>На выход возвращает пути к созданным json и xml файлам.</li>
    </ul>

    <h3>Ресурсы:</h3>
    <ul>
        <li><a href="/test/offices.txt">Файл</a> в котором хранятся офисы.</li>
        <li><a href="/test/offices.json">Пример</a> json в который надо будет преобразовать офисы.</li>
        <li><a href="/test/offices.xml">Пример</a> xml в который надо будет преобразовать офисы.</li>
    </ul>

<?php
$pathFile = __DIR__ . '/test/offices.txt';

$obTransformingFile = new TransformingFile();

$obTransformingFile->convertFileOfficesTxt($pathFile, new Xml());
$obTransformingFile->convertFileOfficesTxt($pathFile, new Json());

$arPathConvertedFiles = $obTransformingFile->getPathConvertedFiles();
if (!empty($arPathConvertedFiles)) {
    ?>
    <h3>Решение, преобразованные файлы:</h3>
    <ul>
        <?php
        foreach ($arPathConvertedFiles as $type => $link) {
            ?>
            <li><a href="<?= $link ?>" download>файл <?= $type ?></a></li>
            <?php
        }
        ?>
    </ul>
    <?php
}
