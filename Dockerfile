FROM php:7.4-apache

RUN mkdir -p /var/www/project

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

COPY . /var/www/project

RUN chown -R www-data:www-data /var/www/project

EXPOSE 80

CMD ["apache2-foreground"]

